no_of_producer,no_of_consumers = map(int,input().split())
producers =  []                                                                                                         
consumers = []                                                                                                          
for i in range(no_of_producer):                                                                                             
    price,day = map(int,input().split())                                                                                    
    producers.append([price,day])                                                                                       
for i in range(no_of_consumers):                                                                                            
    price,day = map(int,input().split())                                                                                    
    consumers.append([price,day])                                                                                       
maxprofit = 0                                                                                                           
for sell in producers:                                                                                                      
    for buy in consumers:                                                                                                       
        if(sell[0]>buy[0] or sell[1]>buy[1]):                                                                                       
            continue                                                                                                            
        current_profit = (buy[0]-sell[0])*(buy[1]-sell[1])                                                                      
        if(current_profit > maxprofit):                                                                                             
            maxprofit = current_profit
print(maxprofit)
